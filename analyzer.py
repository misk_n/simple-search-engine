import regex
import unidecode
from connector import Document


class TokenizedDocument:

    def __init__(self, words, url):
        self.url = url
        self.words = words

    def __str__(self):
        string = self.url + ": "
        for w in self.words:
            string = string + " " + w
        return string


class TextProcessor:
    def process(self, word):
        ...


def analyze(document: Document, processors):
    words = regex.compile('\W').split(document.text)
    words = list(filter(lambda w: w != '', words))
    for processor in processors:
        words = list(map(lambda w: processor.process(processor, word=w), words))

    return TokenizedDocument(words, document.url)


class Normalizer(TextProcessor):
    def process(self, word):
        return unidecode.unidecode(word).lower()
