class Node:
    left = None
    right = None

    def eval(self, index):
        ...


class Leaf(Node):
    def __init__(self, word):
        self.word = word

    def eval(self, index):
        return index.search(self.word)

    def __str__(self):
        return self.word


class And(Node):
    def __init__(self, left, right):
        self.left = left
        self.right = right

    def eval(self, index):
        left = self.left.eval(index)
        right = self.right.eval(index)
        result = []
        for v in left:
            if v in right:
                result.append(v)
                right.remove(v)
        for v in right:
            if v in left:
                result.append(v)
        return result

    def __str__(self):
        return "(" + str(self.left) + " and " + str(self.right) + ")"


class Or(Node):
    def __init__(self, left, right):
        self.left = left
        self.right = right

    def eval(self, index):
        left = self.left.eval(index)
        right = self.right.eval(index)
        result = []
        result += left
        for v in right:
            if v not in result:
                result.append(v)
        return result

    def __str__(self):
        return "(" + str(self.left) + " or " + str(self.right) + ")"


reserved_keywords = {
    'AND': 2,
    'OR': 1,
}


def shunting_yard_algorithm(tokens):
    stack = []
    out = []
    for token in tokens:
        if token not in reserved_keywords:
            out.append(token)
            continue
        # operator
        while len(stack) != 0 and stack[-1] in reserved_keywords and reserved_keywords[stack[-1]] > reserved_keywords[token]:
            out.append(stack.pop())
        stack.append(token)
    while len(stack) != 0:
        out.append(stack.pop())
    return out


def create_request_ast_rec(rpn):
    token = rpn.pop()
    if token not in reserved_keywords:
        return rpn, Leaf(token)
    rpn, left = create_request_ast_rec(rpn)
    rpn, right = create_request_ast_rec(rpn)
    if token == 'AND':
        return rpn, And(left, right)
    else:
        return rpn, Or(left, right)


def eval_request(tokens, index):
    rpn = shunting_yard_algorithm(tokens)
    rpn, ast = create_request_ast_rec(rpn)
    if len(rpn) != 0:
        return None
    return ast.eval(index)
