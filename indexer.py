import bisect


class Posting:
    word = ""
    urls = []

    def __init__(self, word, urls):
        self.word = word
        self.urls = urls


def index(documents):
    res = []
    tmp = {}
    for tdoc in documents:
        for word in tdoc.words:
            if word not in tmp:
                tmp[word] = [tdoc.url]
            elif tdoc.url not in tmp[word]:
                tmp[word].append(tdoc.url)
    for key, val in tmp.items():
        res.append(Posting(key, val))
    return res


last = 0


def new_id():
    global last
    res = last
    last = last + 1
    return res


class Generation:
    wordToDids = {}

    def __init__(self, parent_index, postings):
        self.wordToDids = {}
        for p in postings:
            for u in p.urls:
                if u not in parent_index.urlToDid:
                    parent_index.urlToDid[u] = new_id()
                did = parent_index.urlToDid[u]
                if p.word not in self.wordToDids:
                    self.wordToDids[p.word] = []
                if did not in self.wordToDids[p.word]:  # TODO: sorted/binary search
                    self.wordToDids[p.word].append(did)
                parent_index.support[did] = self  # adding a word to a document

    def search(self, word):
        if word not in self.wordToDids:
            return []
        return self.wordToDids[word]

    def remove_invalid(self, support):
        to_remove = []
        for word, dids in self.wordToDids.items():
            res = []
            for did in dids:
                if support[did] == self:
                    res.append(did)
            if len(res) == 0:
                to_remove.append(word)
            else:
                self.wordToDids[word] = res
        for w in to_remove:
            del self.wordToDids[w]


def true_condition(e):
    return True


def keep_cond(support, gen, did):
    return support[did] == gen


def merge_sorted(l1, l2, condition1=true_condition, condition2=true_condition):
    res = []
    while len(l1) != 0 and len(l2) != 0:
        if l1[0] == l2[0]:
            if condition1(l1[0]):
                res.append(l1[0])
            l1 = l1[1:]
            l2 = l2[1:]
        elif l1[0] < l2[0]:
            if condition1(l1[0]):
                res.append(l1[0])
            l1 = l1[1:]
        else:
            if condition2(l2[0]):
                res.append(l2[0])
            l2 = l2[1:]
    for e in l1:
        if condition1(e):
            res.append(e)
    for e in l2:
        if condition2(e):
            res.append(e)
    return res


class Index:
    urlToDid = {}
    generations = []
    support = {}

    def __init__(self, postings):
        self.add_generation(postings)

    def search_id(self, word):
        res = []
        for g in self.generations:
            l = g.search(word)
            filtered = []
            for did in l:  # TODO: use merge sorted to filter
                if self.support[did] == g:
                    filtered.append(did)
            res = merge_sorted(res, filtered)
        return res

    def search(self, word):
        res = self.search_id(word)
        urls = []
        for did in res:  # TODO: revert dict
            for key, val in self.urlToDid.items():
                if val == did:
                    urls.append(key)
                    break
        return urls

    def add_generation(self, postings):
        self.generations.append(Generation(self, postings))

    def merge_all(self):
        next = Generation(self, [])
        for gen in self.generations:
            gen.remove_invalid(self.support)
            for word, dids in gen.wordToDids.items():
                if word not in next.wordToDids:
                    next.wordToDids[word] = []
                for did in dids:
                    bisect.insort(next.wordToDids[word], did)
        to_delete = []
        for did in self.support:
            if self.support[did] == None:
                to_delete.append(did)
            else:
                self.support[did] = next
        for did in to_delete:
            del self.support[did]
        self.generations = [next]

    def delete(self, url):
        did = self.urlToDid[url]
        self.support[did] = None
