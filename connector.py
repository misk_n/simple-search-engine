import glob
from os import path

class Document:
    text = ""
    url = ""

    def __init__(self, text, url):
        self.text = text
        self.url = url


def fetch(fetch_path, recursive):
    filenames = glob.glob(fetch_path + '/**', recursive=recursive)
    documents = []
    for filename in filenames:
        if path.isdir(filename):
            continue
        f = open(filename, "r")
        try:
            document = Document(f.read(), filename)
            documents.append(document)
        except UnicodeDecodeError:
            continue
    return documents
