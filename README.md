# Simple Search Engine

This is a school project. The main idea is to implement a simple search engine
concept using python. Though theorically scalable, it lacks lots of improvements
to show any performance.

I do not recommand using this software for any other purpose than learning.

## Usage

The main script is engine.py. It will index documents found in the folder given 
in parameters, and allow you to perform search requests.

```
$ python3 engine.py <doc_dir>
> initializing... done
> The requests are formed by the keywords you want to search, separated by the operators OR or AND
> Please enter your request, or q to quit:
  ...
```

