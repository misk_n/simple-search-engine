import pickle
import sys
import ast
from connector import fetch
from analyzer import analyze, Normalizer
from indexer import index, Index


# General purpose read an write functions from/on files
# Can be used to serialize/deserialize a Generation, an Index or an entire search engine...
def save(obj, filename):
    file = open(filename, 'wb')
    pickle.dump(obj, file)


def load(filename):
    file = open(filename, 'rb')
    return pickle.load(file)


# This object represents the entire search engine structure (indexer, document cache, serializers...)
# It is not an accurate representation of a search engine, but it simplifies the implementation for this educational
# purpose project
class Engine:

    def __init__(self, source_directory):
        print(" - normalizing documents...")
        token_documents = list(map(lambda doc: analyze(doc, [Normalizer]), fetch(source_directory, True)))
        self.source_directory = source_directory
        print(" - generating postings... (You can have a coffee, this is quite long)")
        postings = index(token_documents)
        print(" - building the index...")
        self.index = Index(postings)

    def update_index(self, postings):
        self.index.add_generation(postings)
        if len(self.index.generations) > 2:  # Keep only 2 generations, Algolia style ;)
            self.index.merge_all()

    def delete(self, token_document):
        self.index.delete(token_document.url)

    def process_request(self, request):
        tokens = request.split()

        def process(t):
            normalizer = Normalizer()
            if t != 'AND' and t != 'OR':
                return normalizer.process(t)
            return t
        tokens = list(map(lambda t: process(t), tokens))
        return ast.eval_request(tokens, self.index)


# Example purpose main function
def main():
    print("  _____                     _       ______             _               __             _____                            _")
    print(" / ____|                   | |     |  ____|           (_)             / _|           |  __ \                          (_)")
    print("| (___   ___  __ _ _ __ ___| |__   | |__   _ __   __ _ _ _ __   ___  | |_ ___  _ __  | |  | |_   _ _ __ ___  _ __ ___  _  ___  ___")
    print(" \___ \ / _ \/ _` | '__/ __| '_ \  |  __| | '_ \ / _` | | '_ \ / _ \ |  _/ _ \| '__| | |  | | | | | '_ ` _ \| '_ ` _ \| |/ _ \/ __|")
    print(" ____) |  __/ (_| | | | (__| | | | | |____| | | | (_| | | | | |  __/ | || (_) | |    | |__| | |_| | | | | | | | | | | | |  __/\__ \\")
    print("|_____/ \___|\__,_|_|  \___|_| |_| |______|_| |_|\__, |_|_| |_|\___| |_| \___/|_|    |_____/ \__,_|_| |_| |_|_| |_| |_|_|\___||___/")
    print("                                                  __/ |                                                                            ")
    print("                                                 |___/                                                         Will never scale!")
    print()

    if len(sys.argv) != 2:
        print("usage : engine.py [src_dir]")
        return 1

    print("initializing...")
    src_dir = sys.argv[-1]
    engine = Engine(src_dir)
    print("done!")
    print("The requests are formed by the keywords you want to search, separated by the operators OR or AND")
    while True:
        print('PLease enter your request, or q to quit:')
        request = input()
        if request == "q":
            return
        res = engine.process_request(request)
        if res != None:
            print('results:')
            print(res)
        else:
            print("Invalid request: the requests are formed by the keywords you want to search, separated by the operators OR or AND")

    # Bonuses :
    # - incremental indexation             [OK]
    # - AST parsing                        [OK]
    # - meta-data                          [KO]
    # - suppression                        [OK]
    # - positions => ranking               [KO]


main()
